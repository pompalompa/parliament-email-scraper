#Import requests and BeautifulSoup
import requests
import bs4

#URLs of the Parliamentarians web pages.
base_url = 'http://www.parl.gc.ca'
url = 'http://www.parl.gc.ca/Parliamentarians/en/members?view=ListAll'
keep = 'Parliamentarians/en/members/'

#Returns a list of all the hyperlinks in the page.
def get_urls(url):
    links = []
    response = requests.get(url)
    soup = bs4.BeautifulSoup(response.text)
    for link in soup.find_all('a'):
        links.append(link.get('href'))
    return links

#Returns only the hyperlinks with the keep string in them.
def stripper(keep, use_list):
    final_list = []
    for c in use_list:
        if keep in str(c):
            if 'export' not in str(c):
                if 'addresses' not in str(c):
                    final_list.append(c)
    return final_list

#Returns the actual email address.
def go_to(base, links):
    final_links = []
    go_url = ''
    for c in links:
        go_url = base + c
        print("GOING TO: " + go_url)
        response = requests.get(go_url)
        soup = bs4.BeautifulSoup(response.content)
        for link in soup.find_all('a'):
            final_links.append(link.get('href'))

    return stripper('mailto', final_links)

#Creates a file with the email addresses and a mailto infront. Convenient
#as a link to mail all Parliamentarians en masse.
def to_file(email_list, file_name):
    the_file = open(file_name, 'a')
    the_file.write('mailto:?bcc=')
    for c in email_list:
        the_file.write(c + ',')
    the_file.close()
    return 0

#Removes a specified number of letters from strings in a list.
def remover(the_list, letters_to_remove):
    final_list = []
    for c in the_list:
        i = 1
        word_buffer = ''
        for a in c:
            if i > letters_to_remove:
                word_buffer = word_buffer + a
            i = i+1
        final_list.append(word_buffer)
    return final_list

#Retrieve all hyperlinks on the page.
links = get_urls(url)

#Keep only the hyperlinks that lead to a Parliamentarians' page.
good_links = stripper(keep, links)

#This is test data.
#good_links = ['/Parliamentarians/en/members/Aboultaif(89156)']

#Grab the emails
mailto_emails = go_to(base_url, good_links)

#Remove the mailto: so the emails are simply a list.
emails = remover(mailto_emails, 7)

#Write emails to a file.
to_file(emails, 'emails.txt')
